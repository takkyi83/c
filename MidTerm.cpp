```C++
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

# define TOTAL_CARDS_IN_A_DECK (52)

//////////////////// void: declare method 
void PrintCardDeck(struct Card*carddeck); //how to print pokers
struct Card * Shuffle2(struct Card * carddeck, int round);
void setCard(struct Card *card_deck); //reset poker order as new deck

// Poker points
//enum : list stuff 
enum CARDPOINT{
	ACE = 1,TWO = 2, THREE = 3,
	FOUR = 4, FIVE = 5, SIX = 6,
	SEVEN = 7, EIGHT = 8, NINE = 9,
	TEN = 10, JACK = 11, QUEEN = 12,
	KING = 13
};

const int NumCardPoints = KING - ACE + 1;  // the length of card deck numbers

enum CARDTYPE{
	SPADE = 1, DIAMOND = 2, HEART = 3, CLUB = 4 
};

const int NumCardTypes = CLUB - SPADE +1;  

// a card has pattern and point 
struct Card{
	CARDTYPE card_type;
	CARDPOINT card_point;
};

int main(){
	struct Card card_deck[TOTAL_CARDS_IN_A_DECK];
	
	//1. setCard
	setCard(card_deck);
	//2. Shuffle
	PrintCardDeck(Shuffle2(card_deck,100));

	
	return 0;
}

//////////////// the method does what..
//re-order the deck
void setCard(struct Card *card_deck){
	printf("\n order the deck");
	
	for(int i=0; i<NumCardTypes; i++){ //order pattern  0-3    //NumCardTypes = 4 
	    for(int j=0; j<NumCardPoints; j++){  //order numbers  0-12
	        int tempInt = i*NumCardPoints+j;
	        card_deck[tempInt].card_type = (CARDTYPE)(i+1);  //card_deck[i*NumCardPoints+j].card_type --> find position
	        //= (CARDTYPE)(i+1); --> assign number to the position
	        // .card_type --> defined in 'struct' already
	        card_deck[tempInt].card_point = (CARDPOINT)(j+1);
		}
	}

//print the card deck
    printf("\nAfter Setup the Initial Card Deck: ");
    PrintCardDeck(card_deck);
}
// print poker
void PrintCardDeck(struct Card* carddeck){
    printf("\nThe Card Deck: \n");
    char cardtypenamestrings[][10] = {"","clubs", "diamonds","hearts","spades"}; //define length  [10] bytes of each element  
    // the defined number and the position is "1" difference. therefore, leave blank- ""
    char cardpointnamestrings[][5] = {"X","A","2","3","4","5","6","7","8","9","10","J","Q","K"};//define print numbers
    
    for(int x=0; x<TOTAL_CARDS_IN_A_DECK; x++){
    	printf("%s%s\t",
		    cardtypenamestrings[carddeck[x].card_type],  
		    cardpointnamestrings[carddeck[x].card_point]);
		if((x+1)%13==0)
		    printf("\n");
	}
    system("pause");
}

struct Card * Shuffle2(struct Card *carddeck, int round){
	printf("\n shuffle2");
	srand(time(NULL));
	for(int k=0; k<round; k++){
		// int rn = random_num  --> Variable Abbreviation 
		int random_num = ((rand()%49)+1); //mode 49 + 1 = [1..49] 
		//printf("%d ",random_num;
		int location[3] = {random_num, random_num+1, random_num+2};  // take three cards
		struct Card temp_card[3] = {carddeck[location[0]],carddeck[location[1]],carddeck[location[2]]};
		// shift cards from below to above
		for(int i=location[2]; i-3>=0; i--)
		    carddeck[i] = carddeck[i-3];
		// insert first three cards
		for(int i=0; i<3; i++)
		    carddeck[i] = temp_card[i];	
	}
	
return carddeck;
}
```


	
